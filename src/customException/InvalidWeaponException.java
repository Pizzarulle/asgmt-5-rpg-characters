package customException;

/**
 * Class extends {@link Exception}
 */
public class InvalidWeaponException extends Exception{

    private InvalidWeaponException(){}

    public InvalidWeaponException(String message){
        super(message);
    }
}
