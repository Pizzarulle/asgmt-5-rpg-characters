package customException;

/**
 * Class extends {@link Exception}
 */
public class InvalidArmorException extends Exception{

    private InvalidArmorException(){}

    public InvalidArmorException(String message){
        super(message);
    }
}
