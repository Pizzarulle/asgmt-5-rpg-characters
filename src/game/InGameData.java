package game;

import classes.equipment.Armor;
import classes.equipment.Weapon;
import classes.heroes.Hero;

import java.util.List;

public class InGameData {

    private Hero hero;
    private final InGameItems gameItems = new InGameItems();

    public List<Weapon> getWeapons(Rarity rarity) {
        return gameItems.getWeapons(rarity);
    }

    public List<Armor> getArmor(Rarity rarity) {
        return gameItems.getArmor(rarity);
    }

    public Hero getHero() {
        return hero;
    }

    public void setHero(Hero hero) {
        this.hero = hero;
    }
}
