package game;

import classes.baseAttributes.BaseAttributes;
import classes.equipment.Armor;
import classes.equipment.Weapon;
import enums.ArmorType;
import enums.Slot;
import enums.WeaponType;

import java.util.ArrayList;
import java.util.List;

public class InGameItems {
    private final List<Weapon> commonWeapons = new ArrayList<>();
    private final List<Weapon> rareWeapons = new ArrayList<>();
    private final List<Weapon> eliteWeapons = new ArrayList<>();

    private final List<Armor> commonArmor = new ArrayList<>();
    private final List<Armor> rareArmor = new ArrayList<>();
    private final List<Armor> eliteArmor = new ArrayList<>();

    public InGameItems() {
        setDummyWeapons();
        setDummyArmor();
    }

    private void setDummyWeapons() {
        setCommonWeapons();
        setRareWeapons();
        setEliteWeapons();
    }
    private void setDummyArmor(){
        setCommonArmor();
        setRareArmor();
        setEliteArmor();
    }

    public List<Weapon> getWeapons(Rarity rarity){
        return switch (rarity) {
            case COMMON -> commonWeapons;
            case RARE -> rareWeapons;
            case ELITE -> eliteWeapons;
        };
    }
    public List<Armor> getArmor(Rarity rarity){
        return switch (rarity) {
            case COMMON -> commonArmor;
            case RARE -> rareArmor;
            case ELITE -> eliteArmor;
        };
    }

    private void populateItems(Weapon weapon, Rarity rarity) {
        switch (rarity) {
            case COMMON -> commonWeapons.add(weapon);
            case RARE -> rareWeapons.add(weapon);
            case ELITE -> eliteWeapons.add(weapon);
        }
    }

    private void populateItems(Armor weapon, Rarity rarity) {
        switch (rarity) {
            case COMMON -> commonArmor.add(weapon);
            case RARE -> rareArmor.add(weapon);
            case ELITE -> eliteArmor.add(weapon);
        }
    }

    private void setCommonWeapons() {
        populateItems(new Weapon(WeaponType.AXE, "Common axe", 1, 7, 1.1), Rarity.COMMON);
        populateItems(new Weapon(WeaponType.BOW, "Bows´s bow", 1, 15, 0.7), Rarity.COMMON);
        populateItems(new Weapon(WeaponType.SWORD, "Sword of Svante", 1, 13, 1.0), Rarity.COMMON);
        populateItems(new Weapon(WeaponType.STAFF, "Staff of Steffe", 1, 2, 3.0), Rarity.COMMON);
    }

    private void setRareWeapons() {
        populateItems(new Weapon(WeaponType.HAMMER, "Rare hammer", 3, 20, 0.2), Rarity.RARE);
        populateItems(new Weapon(WeaponType.BOW, "Arrow's bow", 2, 15, 1.0), Rarity.RARE);
        populateItems(new Weapon(WeaponType.SWORD, "Swardos Longsword", 2, 13, 1.0), Rarity.RARE);
        populateItems(new Weapon(WeaponType.STAFF, "Wizzard staff", 3, 8, 3.0), Rarity.RARE);
    }

    private void setEliteWeapons() {
        populateItems(new Weapon(WeaponType.AXE, "The Axe", 5, 80, 0.2), Rarity.ELITE);
        populateItems(new Weapon(WeaponType.BOW, "Mega Bow", 6, 50, 0.7), Rarity.ELITE);
        populateItems(new Weapon(WeaponType.SWORD, "String", 7, 20, 1.0), Rarity.ELITE);
        populateItems(new Weapon(WeaponType.STAFF, "Staff of Gandalf", 9, 30, 2.0), Rarity.ELITE);
        populateItems(new Weapon(WeaponType.WAND, "Elder wand", 8, 40, 3.0), Rarity.ELITE);
    }

    private void setCommonArmor(){
        populateItems(new Armor(
                        "Puttes plate protector",
                        1,
                        Slot.BODY,
                        ArmorType.PLATE,
                        new BaseAttributes(2, 0, 0)
                )
                ,Rarity.COMMON
        );
        populateItems(new Armor(
                        "Common mail legs",
                        1,
                        Slot.LEGS,
                        ArmorType.MAIL,
                        new BaseAttributes(1, 1, 0)
                )
                ,Rarity.COMMON
        );
    }
    private void setRareArmor(){
        populateItems(new Armor(
                        "Hat of hate",
                        3,
                        Slot.HEAD,
                        ArmorType.CLOTH,
                        new BaseAttributes(4, 0, 10)
                )
                ,Rarity.RARE
        );
        populateItems(new Armor(
                        "Rare mail legs",
                        3,
                        Slot.LEGS,
                        ArmorType.MAIL,
                        new BaseAttributes(2, 4, 0)
                )
                ,Rarity.RARE
        );
    }
    private void setEliteArmor(){
        populateItems(new Armor(
                        "Elite lether pants",
                        4,
                        Slot.LEGS,
                        ArmorType.LEATHER,
                        new BaseAttributes(4, 8, 2)
                )
                ,Rarity.ELITE
        );
        populateItems(new Armor(
                        "Elite plate head",
                        5,
                        Slot.HEAD,
                        ArmorType.PLATE,
                        new BaseAttributes(10, 2, 0)
                )
                ,Rarity.ELITE
        );
    }

}
