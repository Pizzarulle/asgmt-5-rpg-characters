package game;

import classes.equipment.Item;
import classes.heroes.Mage;
import classes.heroes.Ranger;
import classes.heroes.Rogue;
import classes.heroes.Warrior;
import customException.InvalidArmorException;
import customException.InvalidWeaponException;
import enums.Slot;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Game {
    private final Scanner sc = new Scanner(System.in);
    private final InGameData gameData = new InGameData();
    private boolean runGame = true;
    private String heroName;

    public Game() {
        System.out.println("---WELCOME TRAVELER---");
        presentGameMenu();
        switch (sc.nextInt()) {
            case 1 -> presentChoseName();
            case 0 -> presentExitText();
        }
    }

    public void presentExitText() {
        runGame = false;
        System.out.println("---GOODBYE TRAVELER---");
    }

    public void presentGameMenu() {
        System.out.println("1. CREATE HERO");
        System.out.println("0. EXIT GAME");
    }

    public void presentEquipmentOptions() {
        System.out.println("1. WEAPONS");
        System.out.println("2. ARMOR");
        switch (sc.nextInt()) {
            case 1 -> presentWeaponOptions();
            case 2 -> presentArmorOptions();
        }
    }

    public void presentHeroTypes() {
        System.out.println("1. MAGE");
        System.out.println("2. RANGER");
        System.out.println("3. ROGUE");
        System.out.println("4. WARRIOR");
        System.out.println("0. EXIT GAME");

        switch (sc.nextInt()) {
            case 1 -> gameData.setHero(new Mage(heroName));
            case 2 -> gameData.setHero(new Ranger(heroName));
            case 3 -> gameData.setHero(new Rogue(heroName));
            case 4 -> gameData.setHero(new Warrior(heroName));
            case 0 -> presentExitText();
        }

        presentHeroOptions();
    }

    public void presentItems(Rarity rarity) {
        presentItems(rarity, null);
    }

    /**
     * Prints all the available items for that rarity and if its a WEAPON or not.
     * @param rarity {@link Rarity}
     * @param slot {@link Slot}
     */
    public void presentItems(Rarity rarity, Slot slot) {
        List<Item> items = new ArrayList<>((slot == Slot.WEAPON) ? gameData.getWeapons(rarity) : gameData.getArmor(rarity));

        for (int index = 0; index < items.size(); index++) {
            System.out.println(index + ". " + items.get(index).displayItemInfo());
        }
        int pickedItemIndex = sc.nextInt();

        if (pickedItemIndex > -1 && pickedItemIndex < items.size()) {
            try {
                gameData.getHero().equipNewItem(items.get(pickedItemIndex));
            } catch (InvalidWeaponException | InvalidArmorException exception) {
                System.out.println(exception.getMessage());
            }
        }
    }

    public void printRarityOptions() {
        System.out.println("1. COMMON");
        System.out.println("2. RARE");
        System.out.println("3. ELITE");
    }

    public void presentWeaponOptions() {
        printRarityOptions();
        switch (sc.nextInt()) {
            case 1 -> presentItems(Rarity.COMMON, Slot.WEAPON);
            case 2 -> presentItems(Rarity.RARE, Slot.WEAPON);
            case 3 -> presentItems(Rarity.ELITE, Slot.WEAPON);
        }
    }

    public void presentArmorOptions() {
        printRarityOptions();
        switch (sc.nextInt()) {
            case 1 -> presentItems(Rarity.COMMON);
            case 2 -> presentItems(Rarity.RARE);
            case 3 -> presentItems(Rarity.ELITE);
        }
    }

    public void presentHeroOptions() {
        while (runGame) {
            System.out.println("1. LEVEL UP");
            System.out.println("2. EQUIP ITEMS");
            System.out.println("3. SHOW HERO STATS");
            System.out.println("0. EXIT GAME");
            switch (sc.nextInt()) {
                case 1 -> gameData.getHero().levelUp();
                case 2 -> presentEquipmentOptions();
                case 3 -> presentHeroInfo();
                case 0 -> presentExitText();
            }
        }
    }

    public void presentHeroInfo() {
        System.out.println("--HERO--");

        StringBuilder heroInfo = new StringBuilder();
        heroInfo.append("Name: ").append(gameData.getHero().getName());
        heroInfo.append(" | Level: ").append(gameData.getHero().getLevel());
        heroInfo.append(" | Strength: ").append(gameData.getHero().getTotalAttributes().getStrength());
        heroInfo.append(" | Dexterity: ").append(gameData.getHero().getTotalAttributes().getDexterity());
        heroInfo.append(" | Intelligence: ").append(gameData.getHero().getTotalAttributes().getIntelligence());
        heroInfo.append(" | DPS: ").append(String.format("%.3f", gameData.getHero().getDps()));
        System.out.println(heroInfo);
        System.out.println();
    }

    public void presentChoseName() {
        System.out.println("--ENTER HERO NAME ---");
        heroName = sc.next();
        presentHeroTypes();
    }
}
