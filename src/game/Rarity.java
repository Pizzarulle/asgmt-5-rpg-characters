package game;

public enum Rarity {
    COMMON, RARE, ELITE
}
