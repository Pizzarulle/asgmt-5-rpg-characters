package classes.equipment;

import enums.HeroType;
import enums.Slot;

import java.util.ArrayList;
import java.util.List;

public abstract class Item {
    private final String name;
    private final int requiredLevel;
    private final Slot slot;
    private final List<HeroType> usableForHeroes = new ArrayList<>();

    public Item(String name, int requiredLevel, Slot slot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
    }

    /**
     * Sets which heroTypes can equip this item
     */
    protected abstract void setUsableForHeroes();

    /**
     * Returns a list with the available hero types that can equip this item
     * @return {@link List} of {@link HeroType}
     */
    public List<HeroType> getUsableForHeroes() {
        return usableForHeroes;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public Slot getSlot() {
        return slot;
    }


    /**
     * Returns a StringBuilder appended with some item info
     * @return {@link StringBuilder}
     */
    public StringBuilder displayItemInfo() {
        StringBuilder itemInfo = new StringBuilder();
        itemInfo.append("Name: ").append(name);
        itemInfo.append(" | Required Level: ").append(requiredLevel);
        return itemInfo;
    }
}
