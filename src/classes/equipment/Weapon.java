package classes.equipment;

import enums.Slot;
import enums.WeaponType;

public class Weapon extends Item {
    private final WeaponType weaponType;
    private final int damage;
    private final double attackSpeed;

    public Weapon(WeaponType weaponType, String name, int itemLvl, int damage, double attackSpeed) {
        super(name, itemLvl, Slot.WEAPON);
        this.weaponType = weaponType;
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        setUsableForHeroes();
    }

    /**
     * Multiplies the damage with attack speed
     * @return Sum of damage * attack speed
     */
    public double getDps() {
        return damage * attackSpeed;
    }

    @Override
    protected void setUsableForHeroes() {
        UsableForHeroesStrategy.getInstance().setUsableForHeroes(weaponType, getUsableForHeroes());
    }

    @Override
    public StringBuilder displayItemInfo() {
        StringBuilder weaponInfo = super.displayItemInfo();
        weaponInfo.append(" | DPS : ").append(getDps());
        return weaponInfo;
    }
}
