package classes.equipment;

import enums.ArmorType;
import enums.HeroType;
import enums.WeaponType;

import java.util.List;

/**
 * Class that adds {@link HeroType}s to a specific {@link List}
 */
public class UsableForHeroesStrategy {
    private static UsableForHeroesStrategy usableForHeroesFactory = null;

    /**
     * Returns the instance of this class
     * @return {@link UsableForHeroesStrategy}
     */
    public static UsableForHeroesStrategy getInstance()    {
        if (usableForHeroesFactory == null)
            usableForHeroesFactory = new UsableForHeroesStrategy();
        return usableForHeroesFactory;
    }

    /**
     * Adds different {@link HeroType}s to a list depending on the value of armorType-param
     * @param armorType {@link ArmorType}
     * @param usableForHeroes List to add the HeroTypes to
     */
    public void setUsableForHeroes(ArmorType armorType, List<HeroType> usableForHeroes) {

        switch (armorType) {
            case CLOTH -> usableForHeroes.add(HeroType.MAGE);
            case LEATHER -> {
                usableForHeroes.add(HeroType.RANGER);
                usableForHeroes.add(HeroType.ROGUE);
            }
            case MAIL -> {
                usableForHeroes.add(HeroType.RANGER);
                usableForHeroes.add(HeroType.ROGUE);
                usableForHeroes.add(HeroType.WARRIOR);

            }
            case PLATE -> usableForHeroes.add(HeroType.WARRIOR);
        }
    }
    /**
     * Adds different {@link HeroType}s to a list depending on the value of weaponType-param
     * @param weaponType {@link WeaponType}
     * @param usableForHeroes List to add the HeroTypes to
     */
    public void setUsableForHeroes(WeaponType weaponType, List<HeroType> usableForHeroes) {
        switch (weaponType) {
            case WAND, STAFF -> usableForHeroes.add(HeroType.MAGE);
            case BOW -> usableForHeroes.add(HeroType.RANGER);
            case DAGGER -> usableForHeroes.add(HeroType.ROGUE);
            case SWORD -> {
                usableForHeroes.add(HeroType.ROGUE);
                usableForHeroes.add(HeroType.WARRIOR);
            }
            case HAMMER, AXE -> usableForHeroes.add(HeroType.WARRIOR);
        }
    }
}
