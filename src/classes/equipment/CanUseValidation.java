package classes.equipment;

/**
 * Class used to encapsulate the boolean result and any potential error messages for equipping items
 */
public class CanUseValidation {

    private boolean canUse = true;
    private StringBuilder error = new StringBuilder();

    public boolean canUse() {
        return canUse;
    }

    public String getError() {
        return error.toString();
    }

    /**
     * Appends a string message
     * @param message String
     */
    public void appendError(String message){
        error.append(message);
        canUse = false;
    }
}
