package classes.equipment;

import classes.baseAttributes.BaseAttributes;
import enums.ArmorType;
import enums.Slot;

public class Armor extends Item {
    private final ArmorType armorType;
    private final BaseAttributes armorAttributes;

    public Armor(String name, int requiredLevel, Slot slot, ArmorType armorType, BaseAttributes armorAttributes) {
        super(name, requiredLevel, slot);
        this.armorType = armorType;
        this.armorAttributes = armorAttributes;
        setUsableForHeroes();
    }


    /**
     * Returns {@link BaseAttributes}
     * @return {@link BaseAttributes}
     */
    public BaseAttributes getArmorAttributes() {
        return armorAttributes;
    }

    @Override
    protected void setUsableForHeroes() {
        UsableForHeroesStrategy.getInstance().setUsableForHeroes(armorType, getUsableForHeroes());
    }

    @Override
    public StringBuilder displayItemInfo() {
        StringBuilder armorInfo = super.displayItemInfo();
        armorInfo.append(" | ").append(getSlot());
        armorInfo.append(" | Strength: ").append(armorAttributes.getStrength());
        armorInfo.append(" | Dexterity: ").append(armorAttributes.getDexterity());
        armorInfo.append(" | Intelligence: ").append(armorAttributes.getIntelligence());
        return armorInfo;
    }
}
