package classes.baseAttributes;


public class BaseAttributes {
    private int strength;
    private int dexterity;
    private int intelligence;

    /**
     * Constructor that sets strength,dexterity and intelligence to zero
     */
    public BaseAttributes() {
        strength = 0;
        dexterity = 0;
        intelligence = 0;
    }

    /**
     * Constructor that sets strength,dexterity and intelligence
     *
     * @param strength - Value of strength
     * @param dexterity - Value of dexterity
     * @param intelligence - Value of intelligence
     */
    public BaseAttributes(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    /**
     * Increase the current value of strength, dexterity and intelligence
     * @param strength - Int value to be added to strength
     * @param dexterity - Int value to be added to dexterity
     * @param intelligence - Int value to be added to intelligence
     */
    public void increaseBaseAttributes(int strength, int dexterity, int intelligence) {
        this.strength += strength;
        this.dexterity += dexterity;
        this.intelligence += intelligence;
    }

    /**
     * Increase the value of strength, dexterity and intelligence from another BaseAttributes-object
     * @param attributes
     */
    public void increaseBaseAttributes(BaseAttributes attributes) {
        increaseBaseAttributes(
                attributes.getStrength(),
                attributes.getDexterity(),
                attributes.getIntelligence()
        );
    }

    /**
     * Returns the value of strength
     * @return int value of strength
     */
    public int getStrength() {
        return strength;
    }
    /**
     * Returns the value of dexterity
     * @return int value of dexterity
     */
    public int getDexterity() {
        return dexterity;
    }
    /**
     * Returns the value of intelligence
     * @return int value of intelligence
     */
    public int getIntelligence() {
        return intelligence;
    }
}
