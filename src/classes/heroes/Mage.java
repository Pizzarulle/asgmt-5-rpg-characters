package classes.heroes;

import classes.heroes.Hero;
import enums.HeroType;

public class Mage extends Hero {

    /**
     * Constructor that sets herotype to MAGE and the baseAttributes to 1,1,8
     * @param name String
     */
    public Mage(String name) {
        super(name, HeroType.MAGE);
        setBaseAttributes(1, 1, 8);
    }

    /**
     * Returns the total primary attribute value for a Mage i.e. Intelligence
     * @return int
     */
    @Override
    protected int getPrimaryAttribute() {
       return  getTotalAttributes().getIntelligence();
    }

    /**
     * Increment level by one and increases the baseAttributes
     */
    @Override
    public void levelUp() {
        incrementLevel();
        increaseBaseAttributes(1, 1, 5);
    }
}
