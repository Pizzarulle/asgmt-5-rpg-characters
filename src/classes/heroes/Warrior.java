package classes.heroes;

import enums.HeroType;

public class Warrior extends Hero {

    /**
     * Constructor that sets herotype to WARRIOR and the baseAttributes to 5,2,1
     * @param name String
     */
    public Warrior(String name) {
        super(name, HeroType.WARRIOR);
        setBaseAttributes(5, 2, 1);
    }
    /**
     * Returns the total primary attribute value for a Warrior i.e. Strength
     * @return int
     */
    @Override
    protected int getPrimaryAttribute() {
        return getTotalAttributes().getStrength();
    }
    /**
     * Increment level by one and increases the baseAttributes
     */
    @Override
    public void levelUp() {
        incrementLevel();
        increaseBaseAttributes(3, 2, 1);
    }
}
