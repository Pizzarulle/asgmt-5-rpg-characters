package classes.heroes;

import classes.heroes.Hero;
import enums.HeroType;

public class Ranger extends Hero {

    /**
     * Constructor that sets herotype to RANGER and the baseAttributes to 1,7,1
     * @param name String
     */
    public Ranger(String name) {
        super(name, HeroType.RANGER);
        setBaseAttributes(1, 7, 1);
    }

    /**
     * Returns the total primary attribute value for a Ranger i.e. Dexterity
     * @return int
     */
    @Override
    protected int getPrimaryAttribute() {
        return getTotalAttributes().getDexterity();
    }

    /**
     * Increment level by one and increases the baseAttributes
     */
    @Override
    public void levelUp() {
        incrementLevel();
        increaseBaseAttributes(1, 5, 1);
    }
}
