package classes.heroes;

import classes.baseAttributes.BaseAttributes;
import classes.equipment.CanUseValidation;
import classes.equipment.Item;
import classes.equipment.Armor;
import classes.equipment.Weapon;
import customException.InvalidArmorException;
import customException.InvalidWeaponException;
import enums.HeroType;
import enums.Slot;

import java.util.HashMap;
import java.util.Map;

public abstract class Hero {
    private final String name;
    private final HeroType heroType;
    private int level = 1;
    private BaseAttributes baseAttributes;
    private BaseAttributes totalAttributes;
    private final HashMap<Slot, Item> equipments = new HashMap<Slot, Item>();

    public abstract void levelUp();

    protected abstract int getPrimaryAttribute();

    public Hero(String name, HeroType heroType) {
        this.name = name;
        this.heroType = heroType;
    }

    /**
     * Adds/Replaces a new {@link Item} to the hero.
     * @param newItem -{@link Item}
     * @return true if no exceptions are thrown. Return value only used in tests.
     * @throws InvalidArmorException class extending Exception.
     * @throws InvalidWeaponException class extending Exception.
     */
    public boolean equipNewItem(Item newItem) throws InvalidArmorException, InvalidWeaponException {
        CanUseValidation canUseValidation = new CanUseValidation();

        if (newItem.getRequiredLevel() > level) {
            canUseValidation.appendError("Hero level is to low level to use this item");
        }
        if (newItem.getUsableForHeroes().stream().noneMatch(currentHeroType -> currentHeroType == heroType)) {
            canUseValidation.appendError("Hero cant use this item. Item can only be used by: " + newItem.getUsableForHeroes());
        }

        //Check if any errors has occurred.
        if (!canUseValidation.canUse()) {
            if (newItem instanceof Armor) {
                throw new InvalidArmorException(canUseValidation.getError());
            }
            throw new InvalidWeaponException(canUseValidation.getError());
        }

        equipments.put(newItem.getSlot(), newItem);
        if (newItem instanceof Armor) {
            updateTotalAttributes();
        }

        return canUseValidation.canUse();
    }

    /**
     *     Adds every armors attributes and the heroes baseAttributes together and sets the value of totalAttributes.
     */
    public void updateTotalAttributes() {
        BaseAttributes totalAttributes = new BaseAttributes();
        for (Map.Entry<Slot, Item> equipment : equipments.entrySet()) {
            if (equipment.getValue() instanceof Armor) {
                totalAttributes.increaseBaseAttributes(((Armor) equipment.getValue()).getArmorAttributes());
            }
        }
        totalAttributes.increaseBaseAttributes(baseAttributes);
        this.totalAttributes = totalAttributes;
    }

    /**
     * Returns the heroes dps (weapon dps and stats included).
     * @return double
     */
    public double getDps() {
        return getWeaponDps() * (1 + (getPrimaryAttribute() / 100.0));
    }

    /**
     * If a {@link Weapon} is equipped return its dps. If no {@link Weapon} is equipped the return value is 1.
     * @return double
     */
    private double getWeaponDps() {
        if (equipments.get(Slot.WEAPON) != null) {
            return ((Weapon) equipments.get(Slot.WEAPON)).getDps();
        }
        return 1.0;
    }

    protected void incrementLevel() {
        this.level++;
    }

    /**
     * Sets the values of baseAttributes and updates the totalAttributes
     * @param strength Value for strength
     * @param dexterity Value for dexterity
     * @param intelligence Value for intelligence
     */
    protected void setBaseAttributes(int strength, int dexterity, int intelligence) {
        this.baseAttributes = new BaseAttributes(strength, dexterity, intelligence);
        updateTotalAttributes();
    }
    /**
     * Increases the values of baseAttributes and updates the totalAttributes
     * @param strength increase value for strength
     * @param dexterity increase value for dexterity
     * @param intelligence increase value for intelligence
     */
    protected void increaseBaseAttributes(int strength, int dexterity, int intelligence) {
        baseAttributes.increaseBaseAttributes(strength, dexterity, intelligence);
        updateTotalAttributes();
    }

    /**
     * Returns value of name
     * @return String
     */
    public String getName() {
        return name;
    }
    /**
     * Returns value of level
     * @return int
     */
    public int getLevel() {
        return level;
    }
    /**
     * Returns totalAttributes (attributes gained from level and armor)
     * @return {@link BaseAttributes}
     */
    public BaseAttributes getTotalAttributes() {
        return totalAttributes;
    }
    /**
     * Returns baseAttributes (attributes gained from leveling)
     * @return {@link BaseAttributes}
     */
    public BaseAttributes getBaseAttributes() {
        return baseAttributes;
    }
}
