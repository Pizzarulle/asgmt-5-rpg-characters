package classes.heroes;

import enums.HeroType;

public class Rogue extends Hero {

    /**
     * Constructor that sets herotype to ROGUE and the baseAttributes to 2,6,1
     * @param name String
     */
    public Rogue(String name) {
        super(name, HeroType.ROGUE);
        setBaseAttributes(2, 6, 1);
    }

    /**
     * Returns the total primary attribute value for a Rogue i.e. Dexterity
     * @return int
     */
    @Override
    protected int getPrimaryAttribute() {
        return getTotalAttributes().getDexterity();
    }

    /**
     * Increment level by one and increases the baseAttributes
     */
    @Override
    public void levelUp() {
        incrementLevel();
        increaseBaseAttributes(1, 4, 1);
    }
}
