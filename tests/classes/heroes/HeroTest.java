package classes.heroes;

import classes.baseAttributes.BaseAttributes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HeroTest {

    @Test
    public void getLevel_startLevel_shouldPass() {
        //Arrange
        Hero hero = new Mage("Test hero");
        int expected = 1;
        //Act
        int actual = hero.getLevel();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getLevel_levelAfterFirstLevelUp_shouldPass() {
        //Arrange
        Hero hero = new Mage("Test hero");
        int expected = 2;
        //Act
        hero.levelUp();
        int actual = hero.getLevel();
        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getBaseAttributes_mageBaseAttributes_shouldPass() {
        //Arrange
        Hero mage = new Mage("Test Mage");
        int expectedStrength = 1;
        int expectedDexterity = 1;
        int expectedIntelligence = 8;

        //Act
        BaseAttributes baseAttributes = mage.getBaseAttributes();

        int actualStrength = baseAttributes.getStrength();
        int actualDexterity = baseAttributes.getDexterity();
        int actualIntelligence = baseAttributes.getIntelligence();

        //Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    public void getBaseAttributes_rangerBaseAttributes_shouldPass() {

        //Arrange
        Hero ranger = new Ranger("Test Ranger");
        int expectedStrength = 1;
        int expectedDexterity = 7;
        int expectedIntelligence = 1;

        //Act
        BaseAttributes baseAttributes = ranger.getBaseAttributes();

        int actualStrength = baseAttributes.getStrength();
        int actualDexterity = baseAttributes.getDexterity();
        int actualIntelligence = baseAttributes.getIntelligence();

        //Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    public void getBaseAttributes_rogueBaseAttributes_shouldPass() {
        //Arrange
        Hero rogue = new Rogue("Test Rogue");
        int expectedStrength = 2;
        int expectedDexterity = 6;
        int expectedIntelligence = 1;

        //Act
        BaseAttributes baseAttributes = rogue.getBaseAttributes();

        int actualStrength = baseAttributes.getStrength();
        int actualDexterity = baseAttributes.getDexterity();
        int actualIntelligence = baseAttributes.getIntelligence();

        //Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    public void getBaseAttributes_warriorBaseAttributes_shouldPass() {
        //Arrange
        Hero warrior = new Warrior("Test Rogue");
        int expectedStrength = 5;
        int expectedDexterity = 2;
        int expectedIntelligence = 1;

        //Act
        BaseAttributes baseAttributes = warrior.getBaseAttributes();

        int actualStrength = baseAttributes.getStrength();
        int actualDexterity = baseAttributes.getDexterity();
        int actualIntelligence = baseAttributes.getIntelligence();

        //Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    public void getBaseAttributes_mageBaseAttributesAfterLevelUpOnce_shouldBeTrue() {
        //Arrange
        Hero mage = new Mage("Test Mage");
        int expectedStrength = 1 + 1;
        int expectedDexterity = 1 + 1;
        int expectedIntelligence = 8 + 5;

        //Act
        mage.levelUp();
        BaseAttributes baseAttributes = mage.getBaseAttributes();

        int actualStrength = baseAttributes.getStrength();
        int actualDexterity = baseAttributes.getDexterity();
        int actualIntelligence = baseAttributes.getIntelligence();

        //Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    public void getBaseAttributes_rangerBaseAttributesAfterLevelUpOnce_shouldBeTrue() {
        //Arrange
        Hero ranger = new Ranger("Test Ranger");
        int expectedStrength = 1 + 1;
        int expectedDexterity = 7 + 5;
        int expectedIntelligence = 1 + 1;

        //Act
        ranger.levelUp();
        BaseAttributes baseAttributes = ranger.getBaseAttributes();

        int actualStrength = baseAttributes.getStrength();
        int actualDexterity = baseAttributes.getDexterity();
        int actualIntelligence = baseAttributes.getIntelligence();

        //Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    public void getBaseAttributes_rogueBaseAttributesAfterLevelUpOnce_shouldBeTrue() {
        //Arrange
        Hero rogue = new Rogue("Test Rogue");
        int expectedStrength = 2 + 1;
        int expectedDexterity = 6 + 4;
        int expectedIntelligence = 1 + 1;

        //Act
        rogue.levelUp();
        BaseAttributes baseAttributes = rogue.getBaseAttributes();

        int actualStrength = baseAttributes.getStrength();
        int actualDexterity = baseAttributes.getDexterity();
        int actualIntelligence = baseAttributes.getIntelligence();

        //Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }

    @Test
    public void getBaseAttributes_warriorBaseAttributesAfterLevelUpOnce_shouldBeTrue() {
        //Arrange
        Hero warrior = new Warrior("Test Warrior");
        int expectedStrength = 5 + 3;
        int expectedDexterity = 2 + 2;
        int expectedIntelligence = 1 + 1;

        //Act
        warrior.levelUp();
        BaseAttributes baseAttributes = warrior.getBaseAttributes();

        int actualStrength = baseAttributes.getStrength();
        int actualDexterity = baseAttributes.getDexterity();
        int actualIntelligence = baseAttributes.getIntelligence();

        //Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
    }
}