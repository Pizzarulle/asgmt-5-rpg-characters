package classes.equipment;

import classes.baseAttributes.BaseAttributes;
import classes.heroes.Warrior;
import customException.InvalidArmorException;
import customException.InvalidWeaponException;
import enums.ArmorType;
import enums.Slot;
import enums.WeaponType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ItemTest {

    @Test
    public void equipItem_equipHighLvlWeapon_shouldThrowInvalidWeaponException() {
        //Arrange
        Warrior testWarrior = new Warrior("Test Warrior");
        Weapon testWeapon = new Weapon(WeaponType.AXE, "Common axe", 2, 7, 1.1);
        String expected = "Hero level is to low level to use this item";

        //Act
        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> testWarrior.equipNewItem(testWeapon));
        String actual = exception.getMessage();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void equipItem_equipHighLvlArmor_shouldThrowInvalidArmorException() {
        //Arrange
        Warrior testWarrior = new Warrior("Test Warrior");
        Armor testPlateBody = new Armor(
                "Common Plate Body Armor",
                2,
                Slot.BODY,
                ArmorType.PLATE,
                new BaseAttributes(1, 0, 0)
        );
        String expected = "Hero level is to low level to use this item";

        //Act
        Exception exception = assertThrows(InvalidArmorException.class,
                () -> testWarrior.equipNewItem(testPlateBody));
        String actual = exception.getMessage();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void equipItem_equipInvalidWeaponType_shouldThrowInvalidWeaponException() {
        //Arrange
        Warrior testWarrior = new Warrior("Test Warrior");
        Weapon testBow = new Weapon(WeaponType.BOW, "Common Bow", 1, 12, 0.8);
        String expected = "Hero cant use this item. Item can only be used by: [RANGER]";

        //Act
        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> testWarrior.equipNewItem(testBow));
        String actual = exception.getMessage();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void equipItem_equipInValidArmorType_shouldThrowInvalidArmorException() {
        //Arrange
        Warrior testWarrior = new Warrior("Test Warrior");
        Armor testClothHead = new Armor(
                "Common Cloth Head Armor",
                1,
                Slot.HEAD,
                ArmorType.CLOTH,
                new BaseAttributes(0, 0, 5)
        );
        String expected = "Hero cant use this item. Item can only be used by: [MAGE]";

        //Act
        Exception exception = assertThrows(InvalidArmorException.class,
                () -> testWarrior.equipNewItem(testClothHead));
        String actual = exception.getMessage();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void equipItem_equipValidWeaponType_shouldReturnTrue() throws InvalidArmorException, InvalidWeaponException {
        //Arrange
        Warrior testWarrior = new Warrior("Test Warrior");
        Weapon testWeapon = new Weapon(WeaponType.AXE, "Common axe", 1, 7, 1.1);
        boolean expected = true;

        //Act
        boolean actual = testWarrior.equipNewItem(testWeapon);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void equipItem_equipValidArmorType_shouldReturnTrue() throws InvalidArmorException, InvalidWeaponException {
        //Arrange
        Warrior testWarrior = new Warrior("Test Warrior");
        Armor testPlateBody = new Armor(
                "Common Plate Body Armor",
                1,
                Slot.BODY,
                ArmorType.PLATE,
                new BaseAttributes(1, 0, 0)
        );
        boolean expected = true;

        //Act
        boolean actual = testWarrior.equipNewItem(testPlateBody);

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getDps_heroDpsWithoutWeaponOrArmor_shouldReturnDouble() {
        //Arrange
        Warrior testWarrior = new Warrior("Test Warrior");
        double expected = 1 * (1 + (5 / 100.0));

        //Act
        double actual = testWarrior.getDps();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getDps_heroDpsWithValidWeapon_shouldReturnDouble() throws InvalidArmorException, InvalidWeaponException {
        //Arrange
        Warrior testWarrior = new Warrior("Test Warrior");
        Weapon testWeapon = new Weapon(
                WeaponType.AXE,
                "Common axe",
                1,
                7,
                1.1
        );
        testWarrior.equipNewItem(testWeapon);
        double expected = (7 * 1.1) * (1 + (5 / 100.0));

        //Act
        double actual = testWarrior.getDps();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getDps_heroDpsWithValidWeaponAndArmor_shouldReturnDouble() throws InvalidArmorException, InvalidWeaponException {
        //Arrange
        Warrior testWarrior = new Warrior("Test Warrior");
        Weapon testWeapon = new Weapon(WeaponType.AXE, "Common axe", 1, 7, 1.1);
        Armor testPlateBody = new Armor(
                "Common Plate Body Armor",
                1,
                Slot.BODY,
                ArmorType.PLATE,
                new BaseAttributes(1, 0, 0)
        );
        testWarrior.equipNewItem(testWeapon);
        testWarrior.equipNewItem(testPlateBody);
        double expected = (7 * 1.1) * (1 + ((5 + 1) / 100.0));

        //Act
        double actual = testWarrior.getDps();

        //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getDps_heroLevelUpEquipValidWeaponLevelUpAdain_soudlPass() throws InvalidArmorException, InvalidWeaponException {
        Warrior testWarrior = new Warrior("Test Warrior");
        Weapon testWeapon = new Weapon(WeaponType.AXE, "Common axe", 1, 7, 1.1);
        Armor testPlateBody = new Armor(
                "Common Plate Body Armor",
                1,
                Slot.BODY,
                ArmorType.PLATE,
                new BaseAttributes(2, 0, 0)
        );
        testWarrior.levelUp();
        testWarrior.equipNewItem(testWeapon);
        testWarrior.equipNewItem(testPlateBody);
        testWarrior.levelUp();
        testWarrior.levelUp();
        double expected = (7 * 1.1) * (1 + ((14 + 2) / 100.0));

        //Act
        double actual = testWarrior.getDps();

        //Assert
        assertEquals(expected, actual);
    }
}