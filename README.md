# Assignment 5 - RPG Character

## Description

Simple Java console application where you can create a hero, level it up, equip new items and display its stats.

## Dependencies

JUnit 5.

## Installation and run 

Clone project.

Open project in your IDE.

Run project from IDE.

## Maintainers
[@Andreas Hellström](https://www.gitlab.com/pizzarulle)

